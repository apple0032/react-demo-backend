import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Buildings } from './entities/buildings.entity';
import { BuildingsGfa } from './entities/buildings-gfa.entity';
import { EnergyStarRating } from './entities/energy-star-rating.entity';
import { Metrics } from './entities/metrics.entity';
import { BuildingController } from './buildings.controller';
import { BuildingsService } from './buildings.service';

@Module({
  imports: [
    SequelizeModule.forFeature([
      Buildings,
      BuildingsGfa,
      EnergyStarRating,
      Metrics,
    ]),
  ],
  controllers: [BuildingController],
  providers: [BuildingsService],
})
export class BuildingsModule {}
