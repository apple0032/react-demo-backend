import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Buildings } from './entities/buildings.entity';
import { QueryTypes, Op } from 'sequelize';
import { GetBuildingsRequest } from 'src/common/request/get-buildings';

@Injectable()
export class BuildingsService {
  constructor(
    @InjectModel(Buildings)
    private readonly buildingRepository: typeof Buildings,
  ) {}

  async getAverageEUI() {
    const result = await this.buildingRepository.sequelize.query(
      "SELECT    type,    round(avg(eui),3) AS `average_eui`  FROM    (     SELECT        `t`.`OSEBuildingID` AS `id`,        `t`.`PrimaryPropertyType` AS `type`,        `t2`.`electricity` / `t1`.`gfa` AS `eui`      FROM        `buildings` t        LEFT JOIN (         SELECT            `OSEBuildingID` AS `id`,            SUM(`PropertyUseTypeGFA`) AS `gfa`          FROM            `buildings_gfa`          GROUP BY           `OSEBuildingID`       ) t1 ON `t`.`OSEBuildingID` = `t1`.`id`        LEFT JOIN (         SELECT            `OSEBuildingID` AS id,            value AS `electricity`          FROM            metrics          WHERE            metric = 'Electricity'       ) t2 ON `t`.`OSEBuildingID` = `t2`.`id`   )  GROUP BY   `type`",
      {
        type: QueryTypes.SELECT,
      },
    );
    return result;
  }

  async getBuildings(getBuildingsRequest: GetBuildingsRequest) {
    const filter = JSON.parse(JSON.stringify(getBuildingsRequest));
    //console.log('filter:', filter);
    delete filter.per_page;
    delete filter.page_num;
    if (filter.PropertyName) {
      const searchStr = filter.PropertyName;
      filter.PropertyName = {
        [Op.like]: `%${searchStr}%`,
      };
    }
    //console.log('updated filter:', filter);
    const result = await this.buildingRepository.findAndCountAll({
      where: filter || {},
      offset:
        (getBuildingsRequest?.per_page || 10) *
        ((getBuildingsRequest.page_num || 1) - 1),
      limit: getBuildingsRequest?.per_page || 10,
    });

    if (getBuildingsRequest.per_page) {
      (<any>result).num_of_page = Math.ceil(
        result.count / getBuildingsRequest.per_page,
      );
    }

    return result;
  }
}
