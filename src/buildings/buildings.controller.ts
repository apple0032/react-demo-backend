import { Controller, Get, Query } from '@nestjs/common';
import { BuildingsService } from './buildings.service';
import { GetBuildingsRequest } from 'src/common/request/get-buildings';
import { ApiResponse } from '@nestjs/swagger';

@Controller('building')
export class BuildingController {
  constructor(private readonly buildingService: BuildingsService) {}

  @Get('/avgeui')
  @ApiResponse({
    status: 200,
    description: 'Find average EUI',
  })
  async findAvgEUI() {
    return await this.buildingService.getAverageEUI();
  }

  @Get('/find')
  @ApiResponse({
    status: 200,
    description: 'find buildings information with pagination',
  })
  async findBuildings(@Query() getBuildingsRequest: GetBuildingsRequest) {
    return await this.buildingService.getBuildings(getBuildingsRequest);
  }
}
