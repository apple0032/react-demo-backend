import {
  Model,
  Table,
  PrimaryKey,
  Column,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { Buildings } from './buildings.entity';

@Table({
  tableName: 'buildings_gfa',
})
export class BuildingsGfa extends Model {
  @PrimaryKey
  @Column
  OSEBuildingID: number;

  @PrimaryKey
  @Column(DataType.TEXT)
  PropertyUseType: string;

  @Column(DataType.REAL)
  PropertyUseTypeGFA: number;

  @BelongsTo(() => Buildings, { foreignKey: 'OSEBuildingID' })
  building: Buildings;
}
