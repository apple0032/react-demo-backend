import {
  Model,
  Table,
  PrimaryKey,
  Index,
  Column,
  DataType,
} from 'sequelize-typescript';

@Table({
  tableName: 'energy_star_rating',
})
export class EnergyStarRating extends Model {
  @PrimaryKey
  @Index
  @Column
  OSEBuildingID: number;

  @Column(DataType.TEXT)
  YearsENERGYSTARCertified: string;

  @Column(DataType.REAL)
  ENERGYSTARScore: number;
}
