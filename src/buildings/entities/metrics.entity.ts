import {
  Model,
  Table,
  PrimaryKey,
  Column,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { Buildings } from './buildings.entity';

@Table({
  tableName: 'metrics',
})
export class Metrics extends Model {
  @PrimaryKey
  @Column
  OSEBuildingID: number;

  @PrimaryKey
  @Column(DataType.TEXT)
  metric: string;

  @Column(DataType.REAL)
  value: number;

  @Column(DataType.TEXT)
  unit: string;

  @BelongsTo(() => Buildings, { foreignKey: 'OSEBuildingID' })
  building: Buildings;
}
