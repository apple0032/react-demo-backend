import {
  Model,
  Table,
  PrimaryKey,
  Index,
  Column,
  DataType,
  HasMany,
} from 'sequelize-typescript';
import { BuildingsGfa } from './buildings-gfa.entity';
import { Metrics } from './metrics.entity';

@Table({
  tableName: 'buildings',
  timestamps: false,
})
export class Buildings extends Model {
  @PrimaryKey
  @Index
  @Column
  OSEBuildingID: number;

  @Column
  DataYear: number;

  @Column(DataType.TEXT)
  BuildingType: string;

  @Column(DataType.TEXT)
  PrimaryPropertyType: string;

  @Column(DataType.TEXT)
  PropertyName: string;

  @Column(DataType.TEXT)
  Address: string;

  @Column(DataType.TEXT)
  City: string;

  @Column(DataType.TEXT)
  State: string;

  @Column(DataType.REAL)
  ZipCode: number;

  @Column(DataType.TEXT)
  TaxParcelIdentificationNumber: string;

  @Column(DataType.INTEGER)
  CouncilDistrictCode: number;

  @Column(DataType.REAL)
  Latitude: number;

  @Column(DataType.REAL)
  Longitude: number;

  @Column(DataType.INTEGER)
  YearBuilt: number;

  @Column(DataType.REAL)
  NumberofBuildings: number;

  @Column(DataType.INTEGER)
  NumberofFloors: number;

  @HasMany(() => BuildingsGfa, { foreignKey: 'OSEBuildingID' })
  buildingsGfa: BuildingsGfa[];

  @HasMany(() => Metrics, { foreignKey: 'OSEBuildingID' })
  metrics: Metrics[];
}
