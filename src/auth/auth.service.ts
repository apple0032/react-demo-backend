import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(username: string, password: string) {
    const payload = { username: username, sub: username };
    const result = await this.validateUser(username, password);
    if (!result) {
      return { error_message: 'Wrong password' };
    }
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
