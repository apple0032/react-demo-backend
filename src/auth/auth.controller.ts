import { Controller, HttpCode, Post, Body, Response } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public } from 'src/common';
import { LoginRequest } from 'src/common/request/login-request';
import { ApiResponse } from '@nestjs/swagger';
import { Response as ExpressResponse } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(200)
  @Public()
  @ApiResponse({
    status: 200,
    description: 'Login and get the jwt token',
  })
  @Post('/login')
  async login(
    @Body() loginRequest: LoginRequest,
    @Response() res: ExpressResponse,
  ) {
    const result = await this.authService.login(
      loginRequest.username,
      loginRequest.password,
    );
    if ((<any>result)?.error_message) {
      res.status(200);
    }

    return res.send(result);
  }
}
