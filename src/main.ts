import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  SwaggerDocumentOptions,
  DocumentBuilder,
  SwaggerModule,
} from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const nestConfig: any = {};

  if (process.env.DEV_MODE == 'true') {
    nestConfig['cors'] = true;
  }

  const app = await NestFactory.create(AppModule, nestConfig);

  // auto validation
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  // swagger setup
  const options: SwaggerDocumentOptions = {
    deepScanRoutes: true,
  };
  const config = new DocumentBuilder()
    .setTitle('Varadise Backend')
    .setDescription("Varadise's API")
    .setVersion(process.env.npm_package_version)
    .build();
  const document = SwaggerModule.createDocument(app, config, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(4000);
}
bootstrap();
