import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {IsInt, IsNumberString, IsOptional} from 'class-validator';
import { GetBuildingsFilter } from './get-buildings-filter';

export class GetBuildingsRequest extends GetBuildingsFilter {
  // @IsInt()
  // @IsOptional()
  // @ApiProperty({
  //   description: 'filter',
  //   example: '{}',
  // })
  // filter?: GetBuildingsFilter;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'number of items per pages',
    example: '10',
  })
  @ApiPropertyOptional()
  per_page?: number;

  @IsNumberString()
  @ApiProperty({
    description: 'current page number',
    example: '10',
  })
  @ApiPropertyOptional()
  page_num?: number;
}
