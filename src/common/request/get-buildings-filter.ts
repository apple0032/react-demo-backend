import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {IsInt, IsNumber, IsNumberString, IsOptional, IsString} from 'class-validator';

export class GetBuildingsFilter {
  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'OSEBuildingID',
    example: '1',
  })
  @ApiPropertyOptional()
  OSEBuildingID?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'DataYear',
    example: '2023',
  })
  @ApiPropertyOptional()
  DataYear?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'BuildingType',
    example: 'NonResidential',
  })
  @ApiPropertyOptional()
  BuildingType?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'PrimaryPropertyType',
    example: 'Hotel',
  })
  @ApiPropertyOptional()
  PrimaryPropertyType?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'PropertyName',
    example: 'Mayflower park hotel',
  })
  @ApiPropertyOptional()
  PropertyName?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Address',
    example: '405 Olive way',
  })
  @ApiPropertyOptional()
  Address?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'City',
    example: 'Seattle',
  })
  @ApiPropertyOptional()
  City?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'State',
    example: 'WA',
  })
  @ApiPropertyOptional()
  State?: string;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'ZipCode',
    example: '98101.0',
  })
  @ApiPropertyOptional()
  ZipCode?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'TaxParcelIdentificationNumber',
    example: '0659000030',
  })
  @ApiPropertyOptional()
  TaxParcelIdentificationNumber?: string;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'CouncilDistrictCode',
    example: '7',
  })
  @ApiPropertyOptional()
  CouncilDistrictCode?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'Latitude',
    example: '47.6122',
  })
  @ApiPropertyOptional()
  Latitude?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'Longitude',
    example: '-122.33799',
  })
  @ApiPropertyOptional()
  Longitude?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'YearBuilt',
    example: '1927',
  })
  @ApiPropertyOptional()
  YearBuilt?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'NumberofBuildings',
    example: '1.0',
  })
  @ApiPropertyOptional()
  NumberofBuildings?: number;

  @IsNumberString()
  @IsOptional()
  @ApiProperty({
    description: 'NumberofFloors',
    example: '1.0',
  })
  @ApiPropertyOptional()
  NumberofFloors?: number;
}
