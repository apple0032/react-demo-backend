import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class LoginRequest {
  @IsString()
  @ApiProperty({
    description: 'username',
    example: 'admin',
  })
  username: string;

  @IsString()
  @ApiProperty({
    description: 'password',
    example: 'abc123',
  })
  password: string;
}
