import { SequelizeModuleOptions } from '@nestjs/sequelize';

export const databaseConfig: SequelizeModuleOptions = {
  dialect: 'sqlite',
  storage: '.db/data2.db',
  autoLoadModels: true,
  synchronize: true,
};
