# Use an official Node.js runtime as the base image
FROM node:20-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the entire project directory to the container
COPY . .

# Copy the .env file to the container
COPY .env .env

# Expose the desired port (e.g., 4000) for the container
EXPOSE 4000

# Define the command to run the app when the container starts
CMD ["npm", "run", "start:dev"]